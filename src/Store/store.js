import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import reducers from './../Reducers/rootReducer';
import storage from 'redux-persist/lib/storage';

// creating the object from which will be passed to local storage
const persistConfig = {
    key: 'Ombori',
    storage
};

const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(pReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);