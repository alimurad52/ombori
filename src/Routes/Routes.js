import React from 'react';
import { Router, Route } from "react-router-dom";
import history from './history';
import App from "../Layout/App";


//assigning history to window for pushing any route if needed
window.routerHistory = history;

const Routes = () => {
    return (
        <Router history={history}>
            <Route exact path={`/`} component={App} />
        </Router>
    )
};

export default Routes;
