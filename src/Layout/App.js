import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import Loader from "./../Components/Loader/Loader";
import {Container} from "semantic-ui-react";
import Table from "../Components/Table/Table";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoader: true
        };
    }
    componentDidMount() {
        // 3 sec timeout for the loader
        setTimeout(() => {
            this.setState({showLoader: false});
        }, 3000)
    }

    render() {
        return (
            <Container>
                <div className={this.state.showLoader ? '' : 'DisplayNone'}>
                    <Loader />
                </div>
                <div className={this.state.showLoader ? 'DisplayNone' : ''}>
                    <Table />
                </div>
            </Container>
        );
    }
}

export default App;
