import {GET_DATA} from "./actionTypes";
import axios from 'axios';

// prop function to make api call to retrieve data
export const getData = (page) => {
    return (dispatch) => {
        axios.get(`https://reqres.in/api/users?page=${page}`)
            .then((res) => {
                if(res.status === 200) {
                    dispatch(setData(res.data));
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }
};

function setData(res) {
    return {
        type: GET_DATA,
        payload: res
    }
}