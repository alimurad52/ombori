import React from 'react';
import './Loader.css';

function LoaderComponent() {
    return (
        <span className='spinner' />
    );
}

export default LoaderComponent;
