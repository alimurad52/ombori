import React from "react";
import {Header, List, Grid} from "semantic-ui-react";
import {getData} from "../../Actions/dataAction";
import {connect} from 'react-redux';
import ListItem from "../ListItem/ListItem";
import './Table.css';
import InfiniteScroll from 'react-infinite-scroll-component';
import PropTypes from 'prop-types';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            page: 1,
            hasMore: true,
            height: '50vh'
        };
        this.fetchMore = this.fetchMore.bind(this);
    }

    static get propTypes() {
        return {
            data: PropTypes.any,
            getData: PropTypes.func
        };
    }

    componentDidMount() {
        this.props.getData(this.state.page);
    }

    componentDidUpdate(prevProps) {
        // if the prop data is updated
        // update the local states
        if(this.props.data !== prevProps.data) {
            let joined = this.state.data.concat(this.props.data.data);
            this.setState({ data: joined });
            // once all data is retrieved set hasMore for infinite scroll to false
            if(this.props.data.total === this.state.data.length) {
                this.setState({hasMore: false});
            }
        }
    }

    // on scroll end function listener
    fetchMore() {
        //increase page number to retrieve data for next page
        // change the scroll box height to show all the data
        this.setState({
            page: this.state.page+1,
            height: '83vh'
        }, () => this.props.getData(this.state.page));
    }

    render() {
        return (
            <Grid textAlign='center'>
                <Grid.Column mobile={16} tablet={16} computer={8}>
                    <Header as='h1' className="header-table">Users</Header>
                    <List divided relaxed className="contact-list" id='list-component'>
                        <InfiniteScroll
                            next={this.fetchMore}
                            hasMore={this.state.hasMore}
                            endMessage={<h4>No more users...</h4>}
                            height={this.state.height}
                            className={'infinite-scroll-container'}
                            dataLength={this.state.data.length}>
                        {
                            this.state.data.length > 0 ? this.state.data.map((item, i) => {
                                return <List.Item className='list-item' key={i}><ListItem url={item.avatar} name={item.first_name + ' ' + item.last_name} /></List.Item>
                            }) : "No data to show"
                        }
                        </InfiniteScroll>
                    </List>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    let data = state.data.data;
    return {
        data
    };
};
const mapDispatchToProps = dispatch => ({
    getData: (page) => dispatch(getData(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(Table)
