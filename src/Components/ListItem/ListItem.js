import React from "react";
import {Container, Item, Grid} from "semantic-ui-react";
import PropTypes from 'prop-types';


class ListItem extends React.Component {

    static get propTypes() {
        return {
            url: PropTypes.any,
            name: PropTypes.any
        };
    }

    render() {
        return (
            <Container fluid>
                <Grid>
                    <Grid.Column width={4}>
                        <Item.Image circular size='tiny' src={this.props.url} />
                    </Grid.Column>
                    <Grid.Column width={12} textAlign={'left'} verticalAlign={'middle'}>
                        <Item.Header><strong>{this.props.name}</strong></Item.Header>
                    </Grid.Column>
                </Grid>
            </Container>
        )
    }
}

export default ListItem;