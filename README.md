# Ombori React Code Test

## Live Demo

[https://adoring-bartik-553530.netlify.com/](https://adoring-bartik-553530.netlify.com/)

## To Run

- `git clone https://alimurad52@bitbucket.org/alimurad52/ombori.git`
- `npm install`
- `npm start`

## Available routes
`/`